import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class OddEver {

	public static void main(String[] args) {
		Scanner xScanner = new Scanner(System.in);
		ArrayList<Integer> arrayList1 = new ArrayList<Integer>();
		ArrayList<Integer> arrayList2 = new ArrayList<>();

		int n = 0;
		while (n < 10) {
			int i = xScanner.nextInt();
			if (i % 2 == 0) {
				arrayList2.add(i);
			} else {
				arrayList1.add(i);
			}
			n++;
		}

		Integer[] atemp1 = new Integer[arrayList1.size()];
		Integer[] atemp2 = new Integer[arrayList2.size()];
		arrayList1.toArray(atemp1);
		arrayList2.toArray(atemp2);

		Arrays.sort(atemp1);
		Arrays.sort(atemp2);

		int max = Math.max(arrayList1.size(), arrayList2.size());

		for (int i = 0; i < max; i++) {
			if (i >= arrayList1.size()) {
				System.out.print(0);
			} else {
				System.out.print(atemp1[i]);
			}
			System.out.print(" ");
			if (i >= arrayList2.size()) {
				if (i != max - 1)
					System.out.print(0);
			} else {
				System.out.print(atemp2[i]);
			}
			System.out.print(" ");
		}
	}

}
