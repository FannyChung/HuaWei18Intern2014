import java.util.Scanner;

public class Letters {

	public static void main(String[] args) {
		Scanner xScanner = new Scanner(System.in);
		Letters letters = new Letters();
		String string = xScanner.next();
		for (int i = 0; i < string.length(); i++) {
			char c=string.charAt(i);
			int k = letters.trans(c);
			int re = k * k + k + 1;
			int l = re % 52;
			System.out.print(letters.intrans(l));
		}
	}

	private int trans(char a) {
		int i = 0;
		if (a <= 'z' && a >= 'a') {
			return ((int) a) - 96;
		} else if (a <= 'Z' && a >= 'A') {
			return ((int) a) - 65 + 27;
		}
		return i;
	}

	private char intrans(int i) {
		char a = 'a';
		if (i <= 26 && i >= 1) {
			return (char) (i + 96);
		} else if (i <= 52 && i >= 27) {
			return (char) (i + 65 - 27);
		}
		return a;
	}
}
